Распиновку взять отсюда: http://moteware.com/pub-docs/fcd-prg01-f.pdf (на второй страничке первая таблица Pinout, соответствие первой и третьей колонки

Uart на P1.6/ PM_UCA0TXD (пин 19 на отладке) и P1.5/PM_UCA0RXD (пин 20 на отладке) .
TEST — 6-й пин
RST — 7-й пин
VDD — 1
GND — 34

Соответствие ножек контроллера пинам отладки в целом здесь: http://www.terraelectronica.ru/files/modules/te-cc430f51/TE-CC430F51-868_User-Guide.pdf

команда для прошивки: python cc430-bsl -c /dev/ttyX -D --noadg715 -r -e -I -p led.hex

файл с мигалкой светодиодом и hex я прикладываю

#А вот репозиторий программатора: https://github.com/FlyingCampDesign/msp430-bsl-programmer

========================================================================

# for compilation:
sudo apt-get install gcc-msp430

# for flashing
sudo cp -v msp430-bsl /usr/bin	#wrong way, but works

make flash
#enjoy!
