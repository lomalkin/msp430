#include  <msp430.h>

int main(void)
{
	WDTCTL = WDTPW + WDTHOLD;               // Stop watchdog timer
	P1DIR = 0x01; 				// P1.6 output (green LED)
	P1OUT = 0; 				// LED off

	PMAPPWD = 0x02D52;                        // Get write-access to port mapping regs  
	P1MAP5 = PM_UCA0RXD;                      // Map UCA0RXD output to P1.5 
	P1MAP6 = PM_UCA0TXD;                      // Map UCA0TXD output to P1.6 
	PMAPPWD = 0;                              // Lock port mapping registers 
	
	P1DIR |= BIT6;                            // Set P1.6 as TX output
	P1SEL |= BIT5 + BIT6;                     // Select P1.5 & P1.6 to UART function
	
	UCA0CTL1 |= UCSWRST;                      // **Put state machine in reset**
	UCA0CTL1 |= UCSSEL_2;                     // SMCLK
	UCA0BR0 = 9;                              // 1MHz 115200 (see User's Guide)
	UCA0BR1 = 0;                              // 1MHz 115200
	UCA0MCTL |= UCBRS_1 + UCBRF_0;            // Modulation UCBRSx=1, UCBRFx=0
	UCA0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
  
	volatile unsigned long s;
	//unsigned long s1;
	volatile unsigned long i;
  
	for (;;)				// Loop forever
	{
//		volatile unsigned long i;
		//P1OUT ^= 0x01; 			// Toggle P1.6 output (green LED) using exclusive-OR
//		i = 12999;                   	// Delay
//		i = 1000;
		//while (!(UCA0IFG&UCTXIFG));     // Poll TXIFG to until set

//		do (i--);			// busy waiting (bad)
//		while (i != 0);

		for (s=0; s<1300; s++) {}
		if (i % 10 == 0) {
			P1OUT ^= 0x01;
			UCA0TXBUF = 'a';       // TX -> RXed character
		}
//		for (s=0; s<10; s++) {}		P1OUT = 0;
		
		i++;
		/*
		s++;
		if (s % 100 == 0) {
			P1OUT = 0x01;
		}
		if (s % 101 == 0) {
			P1OUT = 0;
		}
		/*if (s == 100000001) {
			s = 0;
		} // */
	}
}
